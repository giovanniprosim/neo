import React from "react";
import { LayoutProps } from "./interfaces";
import { Typography, Grid } from "@mui/material";

const Layout = ({ children, title }: LayoutProps) => {
  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      alignItems="center"
      component="section"
    >
      <Typography variant="h3">{title}</Typography>
      {children}
    </Grid>
  );
};

export default Layout;
