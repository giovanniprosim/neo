import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { getNeos as apiGetNeos, setResponseApi } from "../../services/neoApi";

import { INeos, INeo } from "../../services/neoApi/interfaces";

import { parsedNeosResponseToNeosList } from "./helpers";

import {
  CircularProgress,
  FormControlLabel,
  Switch,
  Typography,
  Grid,
} from "@mui/material";

import NeoCard from "../../components/NeoCard";
import Layout from "../../components/Layout";

const Home = () => {
  const navigate = useNavigate();

  const [neos, setNeos] = useState<INeo[] | null>(null);
  const [errorMsg, setErrorMsg] = useState<string | null>(null);
  const [showWarning, setShowWarning] = useState(true);

  useEffect(() => {
    const getNeos = async () => {
      const response = await apiGetNeos();

      setResponseApi<INeos>(response, {
        onSuccess: (response) =>
          setNeos(parsedNeosResponseToNeosList(response as INeos)),
        onError: (message) => setErrorMsg(message),
      });
    };
    getNeos();
  }, []);

  const onHandleClickAtNeo = ({ id, name }: INeo) => navigate(`/neo/${id}`);

  return (
    <Layout title="List of Neos">
      {!neos ? (
        errorMsg ? (
          <Typography variant="caption">{errorMsg}</Typography>
        ) : (
          <CircularProgress />
        )
      ) : (
        <>
          <Grid container spacing={2}>
            {neos.map((neo) => (
              <NeoCard
                neo={neo}
                key={neo.id}
                onClick={onHandleClickAtNeo}
                showWarning={showWarning}
              />
            ))}
          </Grid>
          <Grid container>
            <FormControlLabel
              control={
                <Switch
                  defaultChecked={showWarning}
                  onChange={() => setShowWarning(!showWarning)}
                />
              }
              label="Mostrar Perigo?"
            />
          </Grid>
        </>
      )}
    </Layout>
  );
};
export default Home;
