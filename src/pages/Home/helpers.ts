import { INeos, INeo } from "../../services/neoApi/interfaces";

export const parsedNeosResponseToNeosList = (response: INeos): INeo[] =>
  Object.values(response.near_earth_objects)[0];
