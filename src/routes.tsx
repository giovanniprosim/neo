import React, { Suspense, lazy } from "react";
import { BrowserRouter, Routes as ReactRoutes, Route } from "react-router-dom";

import Loading from "./components/Loading";

const Home = lazy(() => import("./pages/Home"));
const NeoDetails = lazy(() => import("./pages/NeoDetails"));

const Routes = () => (
  <Suspense fallback={<Loading />}>
    <BrowserRouter>
      <ReactRoutes>
        <Route path="/" element={<Home />} />
        <Route path="/neo/:id" element={<NeoDetails />} />
      </ReactRoutes>
    </BrowserRouter>
  </Suspense>
);

export default Routes;
