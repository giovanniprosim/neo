import React from "react";
import { CardProps } from "./interfaces";
import { Card, CardContent, Grid, Typography } from "@mui/material";
import { TiWarningOutline } from "react-icons/ti";

const NeoCard = ({ neo, onClick, showWarning }: CardProps) => {
  const isPotencialHazardous = neo.is_potentially_hazardous_asteroid;

  const onHandleClick = () => {
    onClick(neo);
  };

  return (
    <Grid
      direction="column"
      container
      item
      xs={12}
      sm={4}
      md={3}
      alignItems="stretch"
    >
      <Card
        sx={{
          cursor: "pointer",
          background: showWarning && isPotencialHazardous ? "#ffdee9" : "white",
        }}
        onClick={onHandleClick}
      >
        <CardContent>
          <Grid container item direction="column" alignItems="center">
            <Typography variant="subtitle2">#{neo.id}</Typography>
            <Typography variant="subtitle1">{neo.name}</Typography>
            {showWarning && (
              <TiWarningOutline
                color={isPotencialHazardous ? "red" : "grey"}
                opacity={isPotencialHazardous ? 1 : 0.25}
              />
            )}
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default NeoCard;
