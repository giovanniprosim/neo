import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { INeo } from "../../services/neoApi/interfaces";
import { getMeasureParsed, getNextCloseAproachData } from "./helpers";

import {
  getNeoById as getApiNeoById,
  setResponseApi,
} from "../../services/neoApi";
import Layout from "../../components/Layout";
import { CircularProgress, Grid, Typography } from "@mui/material";

const NeoDetails = () => {
  const [neo, setNeo] = useState<INeo | null>(null);
  const [errorMsg, setErrorMsg] = useState<string | null>(null);
  const { id } = useParams();

  useEffect(() => {
    if (!id) return;

    const getNeoById = async () => {
      const response = await getApiNeoById(id);

      setResponseApi<INeo>(response, {
        onSuccess: (response) => setNeo(response as INeo),
        onError: (message) => setErrorMsg(message),
      });
    };
    getNeoById();
  }, [id]);

  const getParsedEstimatedDiameter =
    neo && getMeasureParsed(neo.estimated_diameter);

  const isPotentiallyHazardous = neo?.is_potentially_hazardous_asteroid;

  return neo ? (
    <Layout title={`Neo: ${neo.name}`}>
      <Grid
        container
        direction="column"
        alignItems="center"
        textAlign="center"
        spacing={2}
      >
        <Grid item>
          <Typography variant="subtitle1">ID: #{neo.id}</Typography>
        </Grid>

        <Grid item>
          <Typography fontWeight={600}>
            Absolute Magnitude:{" "}
            <span style={{ fontWeight: "normal" }}>
              {neo.absolute_magnitude_h}
            </span>
          </Typography>
        </Grid>

        <Grid item>
          <Typography fontWeight={600}>
            Estimated Diameter ({getParsedEstimatedDiameter?.unitMeasure})
          </Typography>
          <Typography>
            min: {getParsedEstimatedDiameter?.values.min} | max:
            {getParsedEstimatedDiameter?.values.max}
          </Typography>
        </Grid>

        <Grid item>
          <Typography fontWeight={600}>Close Approach Data</Typography>
          <Typography>
            {getNextCloseAproachData(neo.close_approach_data)}
          </Typography>
        </Grid>

        <Grid item>
          <Typography fontWeight={600}>
            Is Potencial Hazardous:{" "}
            <span
              style={{
                background: isPotentiallyHazardous ? "red" : "white",
                color: isPotentiallyHazardous ? "white" : "black",
                fontWeight: "normal",
              }}
            >
              {isPotentiallyHazardous ? "YES!!!" : "no"}
            </span>
          </Typography>
        </Grid>
      </Grid>
    </Layout>
  ) : (
    <Layout title={`Neo Details ${id ? `#${id}` : ""}`}>
      {errorMsg ? (
        <Typography variant="caption">{errorMsg}</Typography>
      ) : (
        <CircularProgress />
      )}
    </Layout>
  );
};

export default NeoDetails;
