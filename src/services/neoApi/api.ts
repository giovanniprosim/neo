import axios from "axios";

export const baseURL = "https://api.nasa.gov";
export const defaultParams = {
  api_key: "DEMO_KEY",
};

export const neoApi = axios.create({
  baseURL,
  params: defaultParams,
});
