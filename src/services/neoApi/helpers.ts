import { IError } from "./interfaces";

interface ReponseCallbacks {
  onSuccess: (response: unknown) => void;
  onError: (message: string) => void;
}

export const setResponseApi = <T>(
  response: T | IError,
  { onSuccess, onError }: ReponseCallbacks
) => {
  const errorResponse = response as IError;
  if (errorResponse.error) {
    onError(errorResponse.error.message);
    return;
  }
  onSuccess(response);
};
