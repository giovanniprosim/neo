export interface INeos {
  element_count: number;
  links: {
    next: string;
    prev: string;
    self: string;
  };
  near_earth_objects: {
    [date: string]: INeo[];
  };
}

export interface INeo {
  absolute_magnitude_h: number;
  close_approach_data: ICloseAproachData[];
  designation: string;
  estimated_diameter: {
    feet: IUnit;
    kilometers: IUnit;
    meters: IUnit;
    miles: IUnit;
  };
  id: string;
  is_potentially_hazardous_asteroid: boolean;
  is_sentry_object: boolean;
  links: any;
  name: string;
  nasa_jpl_url: string;
  neo_reference_id: string;
  orbital_data: IOrbitalData;
}

export interface ICloseAproachData {
  close_approach_date: string;
  close_approach_date_full: string;
  epoch_date_close_approach: number;
  miss_distance: {
    astronomical: string;
    kilometers: string;
    lunar: string;
    miles: string;
  };
  orbiting_body: string;
}

export interface IUnit {
  estimated_diameter_max: number;
  estimated_diameter_min: number;
}

export interface IError {
  error: {
    code: string;
    message: string;
  };
}

export interface INeoApiError {
  response: {
    status: string;
    statusText: string;
  };
  message: string;
}

export interface IOrbitalData {}

export interface GetNeosProps {
  startDate?: Date | string;
  endDate?: Date | string;
}
