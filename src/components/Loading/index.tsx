import React from "react";

import { Box, CircularProgress } from "@mui/material";

const Loading = (): JSX.Element => (
  <Box
    sx={{
      display: "flex",
      width: "100%",
      height: "100%",
      justifyContent: "center",
      alignItems: "center",
    }}
  >
    <CircularProgress />
  </Box>
);

export default Loading;
