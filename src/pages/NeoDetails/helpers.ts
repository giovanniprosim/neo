import { IUnit, ICloseAproachData } from "../../services/neoApi/interfaces";
import { isAfter, format } from "date-fns";
import enUS from "date-fns/locale/en-US";

export const unitMeasure = {
  km: "kilometers",
  mt: "meters",
};

type UnitMeasureKeys = keyof typeof unitMeasure;
type UnitMeasureValues = typeof unitMeasure[UnitMeasureKeys];

interface MeasureParsedReturn {
  unitMeasure: UnitMeasureKeys;
  values: {
    max: string;
    min: string;
  };
}

export const getMeasureParsed = (estimated_diameter: {
  [key: UnitMeasureValues]: IUnit;
}): MeasureParsedReturn => {
  if (estimated_diameter.kilometers.estimated_diameter_max > 1) {
    return {
      unitMeasure: "km",
      values: {
        min: estimated_diameter.kilometers.estimated_diameter_min.toFixed(3),
        max: estimated_diameter.kilometers.estimated_diameter_max.toFixed(3),
      },
    };
  }

  return {
    unitMeasure: "mt",
    values: {
      min: estimated_diameter.meters.estimated_diameter_min.toFixed(3),
      max: estimated_diameter.meters.estimated_diameter_max.toFixed(3),
    },
  };
};

export const getNextCloseAproachData = (
  closeAproachDatas: ICloseAproachData[]
): string => {
  const nextCloseAproachData = closeAproachDatas.find(
    (closeAproachData) =>
      closeAproachData.orbiting_body.toLocaleLowerCase() === "earth" &&
      isAfter(new Date(closeAproachData.close_approach_date), new Date())
  );

  return nextCloseAproachData
    ? format(new Date(nextCloseAproachData.close_approach_date_full), "PPPP", {
        locale: enUS,
      })
    : "Wihtou aproach date with Earth";
};
