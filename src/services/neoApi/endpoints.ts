import { defaultParams, neoApi } from "./api";
import { GetNeosProps, IError, INeo, INeoApiError, INeos } from "./interfaces";
import mockNeo from "./mocks/mockApiNeo.json";

const endPoints = {
  feed: "/neo/rest/v1/feed",
  neoById: (id: string) => `rest/v1/neo/${id}`,
};

const getParams = (params: { [key: string]: unknown }) => ({
  ...defaultParams,
  ...params,
});

const DEFAULT_DATE = "2021-09-30";
export const getNeos = async ({
  startDate = DEFAULT_DATE,
  endDate = DEFAULT_DATE,
}: GetNeosProps = {}): Promise<INeos | IError> => {
  try {
    const response = await neoApi.get<INeos>(`${endPoints.feed}`, {
      params: getParams({
        start_date: startDate,
        end_date: endDate,
      }),
    });
    return response.data;
  } catch (err) {
    const typedError = err as INeoApiError;
    console.error(`Error at getNeos`, err);
    return {
      error: {
        code: typedError.response.status,
        message: `${typedError.response.statusText} (${typedError.message})`,
      },
    } as IError;
  }
};

export const getNeoById = async (id: string): Promise<INeo | IError> => {
  try {
    const response =
      { data: mockNeo } || (await neoApi.get<INeo>(`${endPoints.neoById(id)}`));
    return response.data;
  } catch (err) {
    const typedError = err as INeoApiError;
    console.error(`Error at getNeoById`, err);
    return {
      error: {
        code: typedError.response.status,
        message: `${typedError.response.statusText} (${typedError.message})`,
      },
    } as IError;
  }
};
