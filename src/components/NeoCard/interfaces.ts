import { INeo } from "../../services/neoApi/interfaces";

export interface CardProps {
  neo: INeo;
  onClick: (neo: INeo) => void;
  showWarning?: boolean;
}
